mod error;

use error::Error;

use std::{
    io,
    fs,
    fmt::Display,
    env,
    process,
    collections::HashMap,
    path::{PathBuf, Path},
};

use clap::{App, Arg};
use ron::{self, from_str, to_string};
use chrono::naive::NaiveDateTime;

const VERSION: &str = "0.1.0";
const INPUT_DATE_FORMAT: &str = "%Y%m%d%H%M%S%f";
const OUTPUT_DATE_FORMAT: &str = "%Y-%m-%d %H:%M:%S";


fn main() -> Result<(), Error> {
    let args = App::new("switch-backup")
        .version(VERSION)
        .args(&[
            Arg::with_name("input")
                .short("i")
                .long("input")
                .takes_value(true)
                .multiple(true)
                .required(true)
                .min_values(1),
            Arg::with_name("output")
                .long("output")
                .short("o")
                .takes_value(true)
                .required(true)
                .max_values(1),
            Arg::with_name("id")
                .long("id-file")
                .short("d")
                .takes_value(true)
                .max_values(1),
            Arg::with_name("copy")
                .long("copy")
                .short("c"),
        ]);

    let help = args.clone();
    let workingdir = env::current_dir().unwrap_or_default();
    let matches = args.get_matches();
    let files = matches.values_of("input");
    let output = Path::new(matches.value_of("output").unwrap());

    if !output.exists() {
        fs::create_dir_all(&output)?;
    }

    let dir = format!("{}", workingdir.join("switch-titles.ron").to_str().unwrap_or_default());
    let idfile = matches.value_of("id-file").unwrap_or(&dir);
    let idfile = PathBuf::from(idfile);

    // <ID, Name>
    let mut games: HashMap<String, Option<String>> = HashMap::new();

    if idfile.exists() {
        let data = fs::read_to_string(&idfile)?;
        games = from_str(&data)?;
    }

    if let Some(files) = files {

        // Iterate through the given files, parse their time / ID.
        // If it isn't a known ID prompt for the name / append to 'games'.
        // Then move / rename.
        for file in files {
            let path = Path::new(file);

            if let Some(stem) = path.file_stem() {
                if let Some(stem) = stem.to_str() {
                    let split: Vec<_> = stem.split('-').collect();

                    if split.len() == 2 {
                        let time = split[0];
                        let id = split[1];
                        let dtime = NaiveDateTime::parse_from_str(time, INPUT_DATE_FORMAT)?;
                        let mut name = String::new();

                        if !games.contains_key(id) || games[id] == None {
                            name = input(format!("Unknown ID {}, enter name: ", id))?;
                            if name.len() == 0 {
                                games.insert(id.to_string(), None);
                                continue
                            }
                            println!("Added {}!", name);
                            games.insert(id.to_string(), Some(name.clone()));
                        } else {
                            if let Some(entry) = &games[id] {
                                name = entry.to_string();
                            };
                        }

                        let time = dtime.format(OUTPUT_DATE_FORMAT).to_string();
                        let filetype = path.extension().unwrap_or_default().to_str().unwrap_or_default();
                        let filename = format!("{}.{}", time, filetype);
                        let fullpath = output.join(name).join(filename);
                        let parent = fullpath.parent().unwrap();
                        if !parent.exists() {
                            fs::create_dir_all(&parent)?;
                        }

                        println!("{:?} -> {:?}", path, fullpath);

                        if matches.is_present("copy") {
                            fs::copy(path, &fullpath)?;
                        } else {
                            fs::rename(path, &fullpath)?;
                        }
                    }
                }
            }
        }

        let output = to_string(&games)?;
        fs::write(idfile, &output)?;
    }
    Ok(())
}

pub fn input<S>(prompt: S) -> Result<String, Error>
where S: Display {
    let mut output = String::new();
    println!("{}", prompt);
    io::stdin().read_line(&mut output)?;
    Ok(output.strip_suffix("\n").unwrap_or(&output).to_string())
}
