use std::io;

#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    TheClap(clap::Error),
    Ron(ron::Error),
    Chrono(chrono::ParseError),
}

impl From<clap::Error> for Error {
    fn from(e: clap::Error) -> Error {
        Error::TheClap(e)
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::IO(e)
    }
}

impl From<ron::Error> for Error {
    fn from(e: ron::Error) -> Error {
        Error::Ron(e)
    }
}

impl From<chrono::ParseError> for Error {
    fn from(e: chrono::ParseError) -> Error {
        Error::Chrono(e)
    }
}
