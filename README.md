# switch-backup

A simple command line tool to rename and move switch screenshots / videos into a more organised / legible structure.

# Installing
```
cargo install --git https://gitlab.com/sj1k/switch-backup
```

# Usage

```
switch-backup -i switch_files/*** -o good_files/

# For now multiple *** may be needed to properly recurse into the switches directories.
# Recursive flag coming soon (hopefully)

